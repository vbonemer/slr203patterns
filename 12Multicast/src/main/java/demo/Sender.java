package demo;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.AbstractActor;

import akka.event.Logging;
import akka.event.LoggingAdapter;

import java.util.HashSet;

public final class Sender extends AbstractActor {

    // Messages

    public static final class SaveReference {

        private final ActorRef reference;

        public SaveReference(ActorRef reference) {
            this.reference = reference;
        }

        public ActorRef getReference() { return reference; }

    }

    // Properties

    private ActorRef multicaster = null;
    private ActorRef receiver1 = null;
    private ActorRef receiver2 = null;
    private ActorRef receiver3 = null;

    private int missingRefs = 4;

    private LoggingAdapter log = Logging.getLogger(getContext().getSystem(), getSelf());

    // Constructors

    private Sender() { }

    public static Props createActor() {
        return Props.create(Sender.class, Sender::new);
    }

    // Methods

    @Override
    public Receive createReceive() {
        return receiveBuilder()
            .match(SaveReference.class, this::receiveSaveReference)
            .build();
    }

    private void receiveSaveReference(SaveReference message) {
        ActorRef reference = message.getReference();
        log.info("Received message from {}: ref={}", getSender(), reference);

        // Save reference
        switch (missingRefs) {
            case 4:
                multicaster = reference;
                break;
            case 3:
                receiver1 = reference;
                break;
            case 2:
                receiver2 = reference;
                break;
            case 1:
                receiver3 = reference;
                break;
            default:
                log.error("\tUnexpected message: unneeded reference.");
                return;
        }

        if (--missingRefs == 0) {
            start();
        }

    }

    private void start() {
        // Send create group 1
        HashSet<ActorRef> group1 = new HashSet<ActorRef>(2);
        group1.add(receiver1);
        group1.add(receiver2);

        multicaster.tell(new Multicaster.CreateGroup(1, group1), getSelf());

        // Send create group 2
        HashSet<ActorRef> group2 = new HashSet<ActorRef>(2);
        group2.add(receiver2);
        group2.add(receiver3);

        multicaster.tell(new Multicaster.CreateGroup(2, group2), getSelf());

        // Send message to group 1
        multicaster.tell(
            new Multicaster.TellGroup(1, new Receiver.PrintString("hello")),
            getSelf()
        );
        // Send message to group 2
        multicaster.tell(
            new Multicaster.TellGroup(2, new Receiver.PrintString("world")),
            getSelf()
        );
    }

}