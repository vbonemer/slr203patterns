package demo;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.AbstractActor;

import akka.event.Logging;
import akka.event.LoggingAdapter;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.Collections;
import java.io.Serializable;
import org.apache.commons.lang3.SerializationUtils;

public final class Multicaster extends AbstractActor {

    // Messages

    public static final class CreateGroup {

        private final int id;
        private final Set<ActorRef> actors;

        public CreateGroup(int id, Set<ActorRef> actors) {
            this.id = id;
            this.actors = Collections.unmodifiableSet(actors);
        }

        public int getId() { return id; }

        public Set<ActorRef> getActors() { return actors; }

    }

    public static final class TellGroup {

        private final int id;
        private final Serializable message;

        public TellGroup(int id, Serializable message) {
            this.id = id;
            this.message = SerializationUtils.clone(message);
        }

        public int getId() { return id; }
        public Serializable getMessage() { return message; }

    }

    // Properties

    private HashMap<Integer, HashSet<ActorRef>> groups = new HashMap<Integer, HashSet<ActorRef>>();

    private LoggingAdapter log = Logging.getLogger(getContext().getSystem(), getSelf());

    // Constructors

    private Multicaster() { }

    public static Props createActor() {
        return Props.create(Multicaster.class, Multicaster::new);
    }

    // Methods

    @Override
    public Receive createReceive() {
        return receiveBuilder()
            .match(CreateGroup.class, this::receiveCreateGroup)
            .match(TellGroup.class, this::receiveTellGroup)
            .build();
    }

    private void receiveCreateGroup(CreateGroup message) {
        int groupId = message.getId();
        log.info("Received message from {}: Create gr={}", getSender(), groupId);

        groups.put(groupId, new HashSet(message.getActors()));
    }

    private void receiveTellGroup(TellGroup message) {
        int groupId = message.getId();
        HashSet<ActorRef> receivers = groups.get(groupId);
        log.info("Received message from {}: Tell gr={}", getSender(), groupId);

        if (receivers == null) {
            log.error("\tInvalid group id {}", groupId);
            return;
        }

        // Send message to actors
        for (ActorRef receiver : receivers) {
            receiver.tell(message.getMessage(), getSelf());
        }
    }

}