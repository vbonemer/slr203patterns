package demo;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;

public final class Multicast {
    
    public static void main(String[] args) {
        // Create the system
        ActorSystem system = ActorSystem.create("system");
        
        // Create Sender
        ActorRef sender = system.actorOf(Sender.createActor(), "Sender");
        // Create Multicaster
        ActorRef multicaster = system.actorOf(Multicaster.createActor(), "Multicaster");
        // Create Receiver1
        ActorRef receiver1 = system.actorOf(Receiver.createActor(), "Receiver1");
        // Create Receiver2
        ActorRef receiver2 = system.actorOf(Receiver.createActor(), "Receiver2");
        // Create Receiver3
        ActorRef receiver3 = system.actorOf(Receiver.createActor(), "Receiver3");

        // Send Multicaster reference to Sender
        sender.tell(new Sender.SaveReference(multicaster), ActorRef.noSender());
        // Send Receiver1 reference to Sender
        sender.tell(new Sender.SaveReference(receiver1), ActorRef.noSender());
        // Send Receiver2 reference to Sender
        sender.tell(new Sender.SaveReference(receiver2), ActorRef.noSender());
        // Send Receiver3 reference to Sender
        sender.tell(new Sender.SaveReference(receiver3), ActorRef.noSender());


        // We wait 5 seconds before ending system (by default)
        // But this is not the best solution.
        try {
            waitBeforeTerminate();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            system.terminate();
        }
    }

    private static void waitBeforeTerminate() throws InterruptedException {
        Thread.sleep(5000);
    }
}
