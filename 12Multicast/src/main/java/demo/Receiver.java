package demo;

import akka.actor.Props;
import akka.actor.AbstractActor;

import akka.event.Logging;
import akka.event.LoggingAdapter;

import java.io.Serializable;

public final class Receiver extends AbstractActor {

    // Messages

    public static final class PrintString implements Serializable {

        private static final long serialVersionUID = 202101151850L;

        private final String content;

        public PrintString(String content) {
            this.content = content;
        }

        public String getContent() { return content; }

    }

    // Properties

    LoggingAdapter log = Logging.getLogger(getContext().getSystem(), getSelf());

    // Constructors

    private Receiver() { }

    public static Props createActor() {
        return Props.create(Receiver.class, Receiver::new);
    }

    // Methods

    @Override
    public Receive createReceive() {
        return receiveBuilder()
            .match(PrintString.class, this::receivePrintString)
            .build();
    }

    private void receivePrintString(PrintString message) {
        log.info("Received message from {}: content={}", getSender(), message.getContent());
    }

}