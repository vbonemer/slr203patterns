package demo;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.AbstractActor;

import akka.event.Logging;
import akka.event.LoggingAdapter;

public final class Child extends AbstractActor {

    // Contructors

    private Child() { }

    public static Props createActor() {
        return Props.create(Child.class, Child::new);
    }

    // Methods

    @Override
    public Receive createReceive() {
        return receiveBuilder().build();
    }

}