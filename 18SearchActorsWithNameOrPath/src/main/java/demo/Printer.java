package demo;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.AbstractActor;

import akka.event.Logging;
import akka.event.LoggingAdapter;

import akka.actor.Identify;
import akka.actor.ActorIdentity;

public final class Printer extends AbstractActor {

    // Messages

    public static final class Print { }

    // Properties

    private LoggingAdapter log = Logging.getLogger(getContext().getSystem(), getSelf());

    // Contructors

    private Printer() { }

    public static Props createActor() {
        return Props.create(Printer.class, Printer::new);
    }

    // Methods

    @Override
    public Receive createReceive() {
        return receiveBuilder()
            .match(Print.class, this::receivePrint)
            .match(ActorIdentity.class, this::receiveActorIdentity)
            .build();
    }

    private void receivePrint(Print message) {
        log.info("PRINTING ALL ACTOR PATHS");
        getContext().actorSelection("/*").tell(new Identify(1), getSelf());
    }

    private void receiveActorIdentity(ActorIdentity message) {
        ActorRef reference;
        try {
            reference = message.getActorRef().get();
        } catch (java.util.NoSuchElementException e) {
            return;
        }
        String path = reference.path().toStringWithoutAddress();
        log.info("Received ActorIdentity from {}", path);
        getContext().actorSelection(path + "/*").tell(new Identify(1), getSelf());
    }

}