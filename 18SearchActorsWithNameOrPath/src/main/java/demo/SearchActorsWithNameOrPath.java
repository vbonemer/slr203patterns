package demo;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;

import akka.event.Logging;
import akka.event.LoggingAdapter;

import java.time.Duration;

public final class SearchActorsWithNameOrPath {

    private static LoggingAdapter log = null;
    
    public static void main(String[] args) {
        // Create the system
        ActorSystem system = ActorSystem.create("system");
        log = Logging.getLogger(system, "main");
        
        // Create A
        ActorRef actorA = system.actorOf(Creator.createActor(), "A");

        ActorRef printer = system.actorOf(Printer.createActor(), "Printer");

        system.getScheduler().scheduleOnce(
            Duration.ofMillis(100), actorA, new Creator.CreateChild(), 
            system.getDispatcher(), ActorRef.noSender()
        );

        system.getScheduler().scheduleOnce(
            Duration.ofMillis(200), actorA, new Creator.CreateChild(), 
            system.getDispatcher(), ActorRef.noSender()
        );

        system.getScheduler().scheduleOnce(
            Duration.ofSeconds(2), printer, new Printer.Print(),
            system.getDispatcher(), ActorRef.noSender()
        );

        log.info("SEARCHING ACTORS BY NAME");
        log.info("A path = {}", system.actorSelection("A").pathString());
        log.info("actor1 path = {}", system.actorSelection("actor1").pathString());
        log.info("actor2 path = {}", system.actorSelection("actor2").pathString());

        log.info("SEARCHING ACTORS BY PATH");
        log.info("A path = {}", system.actorSelection("/A").pathString());
        log.info("actor1 path = {}", system.actorSelection("/A/actor1").pathString());
        log.info("actor2 path = {}", system.actorSelection("/A/actor2").pathString());

        // We wait 5 seconds before ending system (by default)
        // But this is not the best solution.
        try {
            waitBeforeTerminate();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            system.terminate();
        }
    }

    private static void waitBeforeTerminate() throws InterruptedException {
        Thread.sleep(5000);
    }

}