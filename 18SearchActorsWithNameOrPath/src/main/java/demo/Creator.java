package demo;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.AbstractActor;

import akka.event.Logging;
import akka.event.LoggingAdapter;

import java.util.HashSet;

public final class Creator extends AbstractActor {

    private static final class ChildIdProvider {
        private int nextValidId = 1;

        private int getValidId() {
            return this.nextValidId++;
        }
    }

    // Messages

    public static final class CreateChild { }

    // Properties

    private HashSet<ActorRef> children = new HashSet<ActorRef>();

    private ChildIdProvider idProvider = new ChildIdProvider();

    // Contructors

    private Creator() { }

    public static Props createActor() {
        return Props.create(Creator.class, Creator::new);
    }

    // Methods

    @Override
    public Receive createReceive() {
        return receiveBuilder()
            .match(CreateChild.class, this::receiveCreateChild)
            .build();
    }

    private void receiveCreateChild(CreateChild message) {
        ActorRef child = getContext().actorOf(Child.createActor(), "actor" + idProvider.getValidId());
        children.add(child);
    }


}