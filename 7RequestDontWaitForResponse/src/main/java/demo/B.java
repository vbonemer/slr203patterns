package demo;

import demo.Messages.Request;
import demo.Messages.Response;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedAbstractActor;

import akka.event.Logging;
import akka.event.LoggingAdapter;

public final class B extends UntypedAbstractActor {

    // Properties

    private LoggingAdapter log = Logging.getLogger(getContext().getSystem(), getSelf());

    // Constructor

    private B() { }

    public static Props createActor() {
        return Props.create(B.class, () -> {
            return new B();
        });
    }

    // Methods

    public void onReceive(Object message) throws Throwable {
        if (message instanceof Request) {
            receiveRequest((Request) message);
        } else {
            log.warning("Received unknown message from {}: {}", getSender(), message);
        }
    }

    private void receiveRequest(Request message) {
        log.info("Received message from {}: id={}", getSender(), message.getId());
        getSender().tell(new Response(message.getId()), getSelf());
    }
}