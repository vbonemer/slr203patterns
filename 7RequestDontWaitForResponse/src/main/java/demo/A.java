package demo;

import demo.Messages.Request;
import demo.Messages.Response;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedAbstractActor;

import akka.event.Logging;
import akka.event.LoggingAdapter;

public final class A extends UntypedAbstractActor {

    // Messages

    public static final class RefMessage {

        private final ActorRef reference;

        RefMessage(ActorRef ref) {
            this.reference = ref;
        }

        public ActorRef getReference() { return this.reference; }

    }

    // Properties

    private LoggingAdapter log = Logging.getLogger(getContext().getSystem(), getSelf());

    private ActorRef actorB = null;

    // Constructor

    private A() { }

    public static Props createActor() {
        return Props.create(A.class, () -> {
            return new A();
        });
    }

    // Methods

    @Override
    public void onReceive(Object message) throws Throwable {
        if (message instanceof RefMessage) {
            this.receiveReference((RefMessage) message);
        } else if (message instanceof Response) {
            this.receiveResponse((Response) message);
        } else {
            log.warning("Received unknown message: {}", message);
        }
    }

    private void sendMessages() {
        for (int i = 0; i < 30; ++i) {
            actorB.tell(new Request(i), getSelf());
        }
    }

    private void receiveReference(RefMessage message) {
        log.info("Received message from {}: reference={}", getSender(), message.getReference());
        this.actorB = message.getReference();
        sendMessages();
    }

    private void receiveResponse(Response message) {
        log.info("Received message from {}: response_id={}", getSender(), message.getId());
    }

}