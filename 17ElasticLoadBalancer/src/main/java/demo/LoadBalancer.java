package demo;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.AbstractActor;

import akka.event.Logging;
import akka.event.LoggingAdapter;

import java.util.LinkedList;
import java.util.HashMap;
import java.util.HashSet;

public final class LoadBalancer extends AbstractActor {

    // Messages

    public static final class FinishedJob {

        String id;

        public FinishedJob(String id) {
            this.id = id;
        }
    }
    
    // Other

    private static final class ChildIdProvider {
        private int nextValidId = 0;

        private int getValidId() {
            return this.nextValidId++;
        }
    }

    // Properties

    private ChildIdProvider idProvider = new ChildIdProvider();

    private int max = -1;

    private LinkedList<ActorRef> receivers = new LinkedList<ActorRef>();

    private HashMap<ActorRef, HashSet<String>> jobs = new HashMap<ActorRef, HashSet<String>>();

    private LoggingAdapter log = Logging.getLogger(getContext().getSystem(), getSelf());

    // Constructors

    private LoadBalancer() { }

    public static Props createActor() {
        return Props.create(LoadBalancer.class, LoadBalancer::new);
    }

    // Methods

    @Override
    public Receive createReceive() {
        return receiveBuilder()
            .match(Integer.class, this::receiveInteger)
            .match(String.class, this::receiveString)
            .match(FinishedJob.class, this::receiveFinishedJob)
            .build();
    }
    
    private void receiveInteger(Integer message) {
        ActorRef sender = getSender();
        log.info("Received message from {}: max={}", sender, message);

        if (this.max != -1) {
            log.error("\tThe maximum number of child actors has already been set.");
            return;
        }

        this.max = message;
    }

    private void receiveString(String message) {
        ActorRef sender = getSender();
        log.info("Received message from {}: content={}", sender, message);
        ActorRef next;

        if (receivers.size() < max) {
            next = getContext().actorOf(
                Receiver.createActor(getSelf()), 
                "Receiver" + idProvider.getValidId()
            );
            jobs.put(next, new HashSet<String>());
        } else {
            next = receivers.pollFirst();
        }

        try {
            next.tell(message, sender);
            jobs.get(next).add(message);
            receivers.add(next);
        } catch (NullPointerException e) {
            if (max == -1) {
                log.error("Received message {} but max is not set.", message);
            } else {
                log.error("Received message {} but no receivers are registered.", message);
            }
        }
    }

    private void receiveFinishedJob(FinishedJob message) {
        ActorRef sender = getSender();
        HashSet<String> activeJobs = jobs.get(sender);
        log.info("Received message from {}: finishedJob", sender);
        activeJobs.remove(message.id);
        
        if (!activeJobs.isEmpty()) { return; }

        receivers.remove(sender);
        jobs.remove(sender);
        getContext().stop(sender);
    }

}