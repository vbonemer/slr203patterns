package demo;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.AbstractActor;

import akka.event.Logging;
import akka.event.LoggingAdapter;

import java.time.Duration;

public final class Receiver extends AbstractActor {

    // Properties

    private ActorRef balancer;

    private LoggingAdapter log = Logging.getLogger(getContext().getSystem(), getSelf());

    // Constructors

    private Receiver(ActorRef balancer) {
        this.balancer = balancer;
    }

    public static Props createActor(ActorRef balancer) {
        return Props.create(Receiver.class, () -> {
            return new Receiver(balancer);
        });
    }

    // Methods

    @Override
    public Receive createReceive() {
        return receiveBuilder()
            .match(String.class, this::receiveString)
            .build();
    }

    @Override
    public void postStop() {
        log.info("Stopped");
    }

    private void receiveString(String message) {
        log.info("Received message from {}: content={}", getSender(), message);

        getContext().getSystem().getScheduler().scheduleOnce(
            Duration.ofMillis(1000), balancer, new LoadBalancer.FinishedJob(message), 
            getContext().getSystem().getDispatcher(), getSelf()
        );
    }
}