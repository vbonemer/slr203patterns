package demo;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;

public final class ElasticLoadBalancer {
    
    public static void main(String[] args) {
        // Create the system
        ActorSystem system = ActorSystem.create("system");
        
        // Create A
        ActorRef actorA = system.actorOf(Sender.createActor(3), "A");

        // Create LoadBalancer
        ActorRef loadBalancer = system.actorOf(LoadBalancer.createActor(), "LoadBalancer");

        // Send max to LoadBalancer
        loadBalancer.tell(2, ActorRef.noSender());

        // Send LoadBalancer reference to A
        actorA.tell(loadBalancer, ActorRef.noSender());

        // We wait 5 seconds before ending system (by default)
        // But this is not the best solution.
        try {
            waitBeforeTerminate();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            system.terminate();
        }
    }

    private static void waitBeforeTerminate() throws InterruptedException {
        Thread.sleep(5000);
    }

}