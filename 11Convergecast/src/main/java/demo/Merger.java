package demo;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.AbstractActor;

import akka.event.Logging;
import akka.event.LoggingAdapter;

import java.util.HashMap;
import java.util.HashSet;

public final class Merger extends AbstractActor {

    // Messages

    public static final class Join { }

    public static final class Unjoin { }

    // Properties

    private ActorRef receiver = null;

    private HashMap<String, HashSet<ActorRef>> mapping = new HashMap<String, HashSet<ActorRef>>();

    private HashSet<ActorRef> subscribers = new HashSet<ActorRef>();

    private final LoggingAdapter log = Logging.getLogger(getContext().getSystem(), getSelf());

    // Constructors

    private Merger() { }

    public static Props createActor() {
        return Props.create(Merger.class, Merger::new);
    }

    // Methods

    @Override
    public Receive createReceive() {
        return receiveBuilder()
            .match(Messages.SaveReference.class, this::receiveSaveReference)
            .match(Join.class, this::receiveJoin)
            .match(Unjoin.class, this::receiveUnjoin)
            .match(Messages.StringMessage.class, this::receiveString)
            .build();
    }

    private void receiveSaveReference(Messages.SaveReference message) {
        ActorRef reference = message.getReference();
        log.info("Received message from {}: ref={}", getSender(), reference);
        this.receiver = reference;
    }

    private void receiveJoin(Join message) {
        log.info("Received message from {}: join", getSender());
        subscribers.add(getSender());
    }

    private void receiveUnjoin(Unjoin message) {
        log.info("Received message from {}: unjoin", getSender());
        ActorRef sender = getSender();

        forgetSubscriber(sender);
        checkMessages();
    }

    private void receiveString(Messages.StringMessage message) {
        String content = message.getContent();
        log.info("Received message from {}: content={}", getSender(), content);

        if (!mapping.containsKey(content)) {
            mapping.put(content, new HashSet<ActorRef>());
        }

        mapping.get(content).add(getSender());
        checkMessage(content);
    }

    private void forgetSubscriber(ActorRef subscriber) {
        subscribers.remove(subscriber);

        for (HashSet<ActorRef> set : mapping.values()) {
            set.remove(subscriber);
        }
    }

    private void checkMessage(String content) {
        if (!mapping.containsKey(content)) { return; }
        if (!mapping.get(content).containsAll(subscribers)) { return; }
        
        sendMessage(content);
        mapping.remove(content);
    }

    private void checkMessages() {
        for (String key : mapping.keySet()) {
            checkMessage(key);
        }
    }

    private void sendMessage(String content) {
        try {
            receiver.tell(new Messages.StringMessage(content), getSelf());
        } catch (NullPointerException e) {
            log.error("Tried to send message before receiver was set up");
        }
    }

}