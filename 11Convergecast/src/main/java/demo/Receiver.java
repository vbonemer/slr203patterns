package demo;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.AbstractActor;

import akka.event.Logging;
import akka.event.LoggingAdapter;

public final class Receiver extends AbstractActor {

    // Properties

    private LoggingAdapter log = Logging.getLogger(getContext().getSystem(), getSelf());

    // Constructors

    private Receiver() { }

    public static Props createActor() {
        return Props.create(Receiver.class, Receiver::new);
    }

    // Methods

    @Override
    public Receive createReceive() {
        return receiveBuilder()
            .match(Messages.StringMessage.class, this::receiveString)
            .build();
    }

    private void receiveString(Messages.StringMessage message) {
        log.info("Received message from {}: content={}", getSender(), message.getContent());
    }
}