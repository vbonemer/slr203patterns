package demo;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;

public final class Convergecast {

    public static void main(String[] args) {
        // Create system
        ActorSystem system = ActorSystem.create("system");

        // Create A
        ActorRef a = system.actorOf(Publisher.createActor(false), "A");
        // Create B
        ActorRef b = system.actorOf(Publisher.createActor(false), "B");
        // Create C
        ActorRef c = system.actorOf(Publisher.createActor(true), "C");
        // Create Merger
        ActorRef merger = system.actorOf(Merger.createActor(), "Merger");
        // Create D
        ActorRef d = system.actorOf(Receiver.createActor(), "D");

        // Send D ref to Merger
        merger.tell(new Messages.SaveReference(d), ActorRef.noSender());
        // Send Merger ref to A
        a.tell(new Messages.SaveReference(merger), ActorRef.noSender());
        // Send Merger ref to B
        b.tell(new Messages.SaveReference(merger), ActorRef.noSender());
        // Send Merger ref to C
        c.tell(new Messages.SaveReference(merger), ActorRef.noSender());


        // We wait 5 seconds before ending system (by default)
        // But this is not the best solution.
        try {
            waitBeforeTerminate();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            system.terminate();
        }
    }

    public static void waitBeforeTerminate() throws InterruptedException {
        Thread.sleep(5000);
    }

}