package demo;

import akka.actor.ActorRef;

public final class Messages {
    public static final class StringMessage {

        private String content;

        public StringMessage(String str) {
            this.content = str;
        }

        public String getContent() { return content; }

    }

    public static final class SaveReference {
        
        private ActorRef reference;

        public SaveReference(ActorRef ref) {
            this.reference = ref;
        }

        public ActorRef getReference() { return reference; }

    }
}