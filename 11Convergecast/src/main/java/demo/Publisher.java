package demo;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.AbstractActor;

import akka.event.Logging;
import akka.event.LoggingAdapter;

import java.time.Duration;
import akka.actor.Scheduler;

public final class Publisher extends AbstractActor {

    // Messages

    public static final class Go { }

    // Properties

    private ActorRef merger = null;

    private boolean isFirstGo = true;

    private final boolean shouldUnjoin;

    private final LoggingAdapter log = Logging.getLogger(getContext().getSystem(), getSelf());

    // Constructors

    private Publisher(boolean unjoinAfterSend) {
        this.shouldUnjoin = unjoinAfterSend;
    }

    public static Props createActor(boolean unjoinAfterSend) {
        return Props.create(Publisher.class, () -> {
            return new Publisher(unjoinAfterSend);
        });
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
            .match(Go.class, this::receiveGo)
            .match(Messages.SaveReference.class, this::receiveSaveReference)
            .build();
    }

    private void receiveGo(Go message) {
        log.info("Received message from {}: go", getSender());

        // Send hi
        String content = isFirstGo ? "hi" : "hi2";
        merger.tell(new Messages.StringMessage(content), getSelf());
        
        if (!isFirstGo) { return; }
        isFirstGo = false;

        if (shouldUnjoin) {
            sendUnjoin();
            return;
        }
        scheduleSecondGo();
    }

    private void receiveSaveReference(Messages.SaveReference message) {
        ActorRef reference = message.getReference();
        log.info("Received message from {}: ref={}", getSender(), reference);

        this.merger = reference;
        sendJoin();
        scheduleGo();
    }

    private void sendJoin() {
        try {
            merger.tell(new Merger.Join(), getSelf());
        } catch (NullPointerException e) {
            log.error("Tried to send message before merger was set up");
        }
    }

    private void sendUnjoin() {
        try {
            merger.tell(new Merger.Unjoin(), getSelf());
        } catch (NullPointerException e) {
            log.error("Tried to send message before merger was set up");
        }
    }

    private void scheduleGo() {
        getContext().getSystem().getScheduler().scheduleOnce(
            Duration.ofMillis(1000), getSelf(), new Go(),
            getContext().getSystem().getDispatcher(), ActorRef.noSender()
        );
    }

    private void scheduleSecondGo() {
        getContext().getSystem().getScheduler().scheduleOnce(
            Duration.ofMillis(1000), getSelf(), new Go(),
            getContext().getSystem().getDispatcher(), ActorRef.noSender()
        );
    }
}