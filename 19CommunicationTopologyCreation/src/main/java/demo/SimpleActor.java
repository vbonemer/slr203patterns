package demo;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.AbstractActor;

import akka.event.Logging;
import akka.event.LoggingAdapter;

import java.util.Collections;
import java.util.List;
import java.util.ArrayList;

public final class SimpleActor extends AbstractActor {

    // Messages

    public static final class SaveReferences {

        public final List references;

        public SaveReferences(ArrayList<ActorRef> references) {
            this.references = Collections.unmodifiableList(references);
        }
    }

    public static final class PrintKnowActors { }

    // Properties

    private List<ActorRef> references = null;

    private LoggingAdapter log = Logging.getLogger(getContext().getSystem(), getSelf());

    // Contructors

    private SimpleActor() { }

    public static Props createActor() {
        return Props.create(SimpleActor.class, SimpleActor::new);
    }

    // Methods

    @Override
    public Receive createReceive() {
        return receiveBuilder()
            .match(SaveReferences.class, this::receiveSaveReferences)
            .match(PrintKnowActors.class, this::receivePrintKnowActors)
            .build();
    }

    private void receiveSaveReferences(SaveReferences message) {
        log.info("Received message from {}: save {} references", getSender(), message.references.size());
        this.references = message.references;
    }

    private void receivePrintKnowActors(PrintKnowActors message) {
        log.info("Received message from {}: print known actors", getSender());
        String content = "";
        for (ActorRef ref : references) {
            content += ref.path().name() + "; ";
        }
        if (content != "") { content = content.substring(0, content.length() - 2); }
        log.info("\t{}", content);
    }

}