package demo;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.AbstractActor;

import akka.event.Logging;
import akka.event.LoggingAdapter;

import java.util.ArrayList;
import java.util.HashSet;
import org.javatuples.Pair;

public final class TopologyGenerator extends AbstractActor {

    public static final class AdjacencyMatrix {

        public final int size;

        private HashSet<Pair<Integer, Integer>> edges = new HashSet<Pair<Integer, Integer>>();

        public AdjacencyMatrix(int size) {
            this.size = size;
        }

        public void addDirectedEdge(int i, int j) {
            Pair<Integer, Integer> index = new Pair<Integer, Integer>(i, j);
            edges.add(index);
        }

        public void addDirectedEdges(int ... values) {
            if (values.length % 2 != 0) {
                throw new RuntimeException("Invalid parameters");
            }
            for (int i = 0; i < values.length; i += 2) {
                Pair<Integer, Integer> index = new Pair<Integer, Integer>(values[i], values[i+1]);
                edges.add(index);
            }
        }

        public boolean hasDirectedEdge(int i, int j) {
            Pair<Integer, Integer> index = new Pair<Integer, Integer>(i, j);
            return edges.contains(index);
        }

    }

    // Messages

    public static final class PrintGraph { }

    // Properties

    private ArrayList<ActorRef> children;

    // Contructors

    private TopologyGenerator(AdjacencyMatrix matrix) {
        // Create the actors
        this.children = new ArrayList<ActorRef>(matrix.size);
        
        for (int i = 0; i < matrix.size; ++i) {
            ActorRef actor = getContext().actorOf(
                SimpleActor.createActor(),
                String.valueOf((char) ('A' + i))
            );
            children.add(i, actor);
        }

        // Send references
        for (int i = 1; i <= matrix.size; ++i) {
            ArrayList<ActorRef> references = new ArrayList<ActorRef>(matrix.size);
            // Get ActorRef of known actors
            for (int j = 1; j <= matrix.size; ++j) {
                if (!matrix.hasDirectedEdge(i, j)) { continue; }
                references.add(children.get(j - 1));
            }
            // Send the list to the appropriate child
            children.get(i - 1).tell(new SimpleActor.SaveReferences(references), getSelf());
        }
    }

    public static Props createActor(AdjacencyMatrix matrix) {
        return Props.create(TopologyGenerator.class, () -> {
            return new TopologyGenerator(matrix);
        });
    }

    // Methods

    @Override
    public Receive createReceive() {
        return receiveBuilder()
            .match(PrintGraph.class, this::receivePrintGraph)
            .build();
    }

    private void receivePrintGraph(PrintGraph message) {
        for (ActorRef child : children) {
            child.tell(new SimpleActor.PrintKnowActors(), getSelf());
        }
    }


}