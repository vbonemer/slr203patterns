package demo;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;

import akka.event.Logging;
import akka.event.LoggingAdapter;

import java.time.Duration;

public final class CommunicationTopologyCreation {
    
    public static void main(String[] args) {
        // Create the system
        ActorSystem system = ActorSystem.create("system");
        
        // Create TopologyGenerator
        TopologyGenerator.AdjacencyMatrix matrix = new TopologyGenerator.AdjacencyMatrix(4);
        matrix.addDirectedEdges(1, 2, 1, 3, 2, 4, 3, 1, 3, 4, 4, 1, 4, 4);
        ActorRef topologyGenerator = system.actorOf(TopologyGenerator.createActor(matrix), "TopologyGenerator");

        system.getScheduler().scheduleOnce(
            Duration.ofMillis(1000), topologyGenerator, new TopologyGenerator.PrintGraph(), 
            system.getDispatcher(), ActorRef.noSender()
        );


        // We wait 5 seconds before ending system (by default)
        // But this is not the best solution.
        try {
            waitBeforeTerminate();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            system.terminate();
        }
    }

    private static void waitBeforeTerminate() throws InterruptedException {
        Thread.sleep(5000);
    }

}