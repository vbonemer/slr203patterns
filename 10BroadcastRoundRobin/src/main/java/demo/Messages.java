package demo;

import akka.actor.ActorRef;

public final class Messages {

    public static final class StringMessage {

        private String content;

        public StringMessage(String content) {
            this.content = content;
        }

        public String getContent() { return content; }

    }

    public static final class RefMessage {

        private ActorRef reference;

        public RefMessage(ActorRef ref) {
            this.reference = ref;
        }

        public ActorRef getReference() {
            return reference;
        }

    }
}