package demo;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.ActorSystem;

public final class BroadcastRoundRobin {

    public static void main(String[] args) {
        // Create System
        ActorSystem system = ActorSystem.create("system");
        
        // Create Broadcaster
        ActorRef broadcaster = system.actorOf(Broadcaster.createActor(), "broadcaster");

        // Create A
        ActorRef actorA = system.actorOf(Publisher.createActor(), "a");

        // Create B
        ActorRef actorB = system.actorOf(Subscriber.createActor(), "b");

        // Create C
        ActorRef actorC = system.actorOf(Subscriber.createActor(), "c");

        // Send Broadcaster ref to A
        actorA.tell(new Messages.RefMessage(broadcaster), ActorRef.noSender());

        // Send Broadcaster ref to B
        actorB.tell(new Messages.RefMessage(broadcaster), ActorRef.noSender());

        // Send Broadcaster ref to C
        actorC.tell(new Messages.RefMessage(broadcaster), ActorRef.noSender());


        // We wait 5 seconds before ending system (by default)
        // But this is not the best solution.
        try {
            waitBeforeTerminate();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            system.terminate();
        }
    }

    public static void waitBeforeTerminate() throws InterruptedException {
        Thread.sleep(5000);
    }
}