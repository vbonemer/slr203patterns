package demo;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.AbstractActor;

import akka.actor.Scheduler;
import java.time.Duration;

import akka.event.Logging;
import akka.event.LoggingAdapter;

import demo.Messages;

public final class Publisher extends AbstractActor {

    // Messages

    public static final class Start { }

    // Properties

    private ActorRef broadcaster = null;

    private LoggingAdapter log = Logging.getLogger(getContext().getSystem(), getSelf());

    // Constructors

    private Publisher() { }

    public static Props createActor() {
        return Props.create(Publisher.class, Publisher::new);
    }

    // Methods

    @Override
    public Receive createReceive() {
        return receiveBuilder()
            .match(Start.class, this::receiveStart)
            .match(Messages.RefMessage .class, this::receiveRef)
            .build();
    }

    private void receiveStart(Start message) {
        log.info("Received message from {}: start", getSender());
        sendMessage();
    }

    private void receiveRef(Messages.RefMessage message) {
        ActorRef reference = message.getReference();
        log.info("Received message from {}: ref={}", getSender(), reference);
        this.broadcaster = reference;

        scheduleStart();
    }

    private void scheduleStart() {
        getContext().getSystem().getScheduler().scheduleOnce(
            Duration.ofMillis(1000), getSelf(), new Start(), 
            getContext().getSystem().getDispatcher(), ActorRef.noSender()
        );
    }

    private void sendMessage() {
        broadcaster.tell(new Messages.StringMessage("m"), getSelf());
    }

}