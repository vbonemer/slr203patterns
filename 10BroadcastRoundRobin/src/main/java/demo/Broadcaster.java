package demo;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.AbstractActor;

import akka.event.Logging;
import akka.event.LoggingAdapter;

import java.util.ArrayList;

public final class Broadcaster extends AbstractActor {

    // Messages

    public static final class Join { }

    // Properties

    private ArrayList<ActorRef> subscribers = new ArrayList<ActorRef>();

    private LoggingAdapter log = Logging.getLogger(getContext().getSystem(), getSelf());

    // Contructors

    private Broadcaster() { }

    public static Props createActor() {
        return Props.create(Broadcaster.class, Broadcaster::new);
    }

    // Methods

    @Override
    public Receive createReceive() {
        return receiveBuilder()
            .match(Join.class, this::receiveJoin)
            .match(Messages.StringMessage.class, this::receiveString)
            .build();
    }

    private void receiveJoin(Join message) {
        ActorRef sender = getSender();
        
        log.info("Received message from {}: join", sender);
        subscribers.add(sender);
    }

    private void receiveString(Messages.StringMessage message) {
        log.info("Received message from {}: content={}", getSender(), message.getContent());
        broadcast(message);
    }

    private void broadcast(Messages.StringMessage message) {
        for (ActorRef subscriber : subscribers) {
            subscriber.tell(message, getSelf());
        }
    }
    

}