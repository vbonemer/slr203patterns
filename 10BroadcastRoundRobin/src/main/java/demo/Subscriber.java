package demo;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.AbstractActor;

import akka.event.Logging;
import akka.event.LoggingAdapter;

public final class Subscriber extends AbstractActor {

    // Properties

    private ActorRef broadcaster = null;

    private LoggingAdapter log = Logging.getLogger(getContext().getSystem(), getSelf());

    // Constructors

    private Subscriber() { }

    public static Props createActor() {
        return Props.create(Subscriber.class, Subscriber::new);
    }

    // Methods

    @Override
    public Receive createReceive() {
        return receiveBuilder()
            .match(Messages.RefMessage.class, this::receiveRef)
            .match(Messages.StringMessage.class, this::receiveString)
            .build();
    }

    private void receiveRef(Messages.RefMessage message) {
        ActorRef reference = message.getReference();
        
        log.info("Received message from {}: ref={}", getSender(), reference);
        this.broadcaster = reference;
        subscribe();
    }

    private void receiveString(Messages.StringMessage message) {
        log.info("Received message from {}: content={}", getSender(), message.getContent());
    }

    private void subscribe() {
        broadcaster.tell(new Broadcaster.Join(), getSelf());
    }
}