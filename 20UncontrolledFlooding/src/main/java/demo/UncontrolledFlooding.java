package demo;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;

import akka.event.Logging;
import akka.event.LoggingAdapter;

import java.time.Duration;

public final class UncontrolledFlooding {
    
    public static void main(String[] args) {
        // Create matrix
        TopologyGenerator.AdjacencyMatrix matrix = new TopologyGenerator.AdjacencyMatrix(5);
        matrix.addDirectedEdges(1, 2, 1, 3, 2, 4, 3, 4, 4, 5);

        { // Begin scope
            // Create the system
            ActorSystem system = ActorSystem.create("systemWithNoCycle");
            
            // Create TopologyGenerator
            ActorRef topologyGenerator = system.actorOf(TopologyGenerator.createActor(matrix), "TopologyGenerator");

            Object message = new TopologyGenerator.TellNode(1, new Node.Packet());
            system.getScheduler().scheduleOnce(
                Duration.ofMillis(1000), topologyGenerator, message, 
                system.getDispatcher(), ActorRef.noSender()
            );


            // We wait 5 seconds before ending system (by default)
            // But this is not the best solution.
            try {
                waitBeforeTerminate();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                system.terminate();
            }
        } // End scope

        { // Begin scope
            // Create the system
            ActorSystem system = ActorSystem.create("systemWithCycle");
            
            // Add edge to matrix
            matrix.addDirectedEdge(5, 2);

            // Create TopologyGenerator
            ActorRef topologyGenerator = system.actorOf(TopologyGenerator.createActor(matrix), "TopologyGenerator");

            Object message = new TopologyGenerator.TellNode(1, new Node.Packet());
            system.getScheduler().scheduleOnce(
                Duration.ofMillis(1000), topologyGenerator, message, 
                system.getDispatcher(), ActorRef.noSender()
            );


            // We wait 5 seconds before ending system (by default)
            // But this is not the best solution.
            try {
                waitBeforeTerminate();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                system.terminate();
            }
        } // End scope
    }

    private static void waitBeforeTerminate() throws InterruptedException {
        Thread.sleep(1200);
    }

}