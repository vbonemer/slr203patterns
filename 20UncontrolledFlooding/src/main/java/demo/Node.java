package demo;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.AbstractActor;

import akka.event.Logging;
import akka.event.LoggingAdapter;

import java.util.Collections;
import java.util.List;
import java.util.ArrayList;
import java.io.Serializable;

public final class Node extends AbstractActor {

    // Messages

    public static final class SaveReferences {

        public final List references;

        public SaveReferences(ArrayList<ActorRef> references) {
            this.references = Collections.unmodifiableList(references);
        }
    }

    public static final class Packet implements Serializable {
        public static final long serailVersionUID = 202101200946L;
    }

    public static final class PrintKnowActors { }

    // Properties

    private List<ActorRef> references = null;

    private LoggingAdapter log = Logging.getLogger(getContext().getSystem(), getSelf());

    // Contructors

    private Node() { }

    public static Props createActor() {
        return Props.create(Node.class, Node::new);
    }

    // Methods

    @Override
    public Receive createReceive() {
        return receiveBuilder()
            .match(SaveReferences.class, this::receiveSaveReferences)
            .match(Packet.class, this::receivePacket)
            .match(PrintKnowActors.class, this::receivePrintKnowActors)
            .build();
    }

    private void receiveSaveReferences(SaveReferences message) {
        log.info("Received message from {}: save {} references", getSender(), message.references.size());
        this.references = message.references;
    }

    private void receivePacket(Packet message) {
        ActorRef sender = getSender();
        log.info("Received message from {}: packet", sender.path().name());
        
        for (ActorRef ref : references) {
            if (ref.equals(sender)) { continue; }
            ref.tell(message, getSelf());
        }
    }

    private void receivePrintKnowActors(PrintKnowActors message) {
        log.info("Received message from {}: print known actors", getSender());
        String content = "";
        for (ActorRef ref : references) {
            content += ref.path().name() + "; ";
        }
        if (content != "") { content = content.substring(0, content.length() - 2); }
        log.info("\t{}", content);
    }

}