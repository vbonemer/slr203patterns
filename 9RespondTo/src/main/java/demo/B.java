package demo;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedAbstractActor;

import akka.event.Logging;
import akka.event.LoggingAdapter;

import akka.pattern.Patterns;
import java.util.concurrent.CompletableFuture;

import demo.C;

public final class B extends UntypedAbstractActor {

    // Messages

    public static final class RequestRefMessage {

        private final int id;
        private final ActorRef reference;

        public RequestRefMessage(int id, ActorRef ref) {
            this.id = id;
            this.reference = ref;
        }

        public int getId() { return id; }
        public ActorRef getReference() { return reference; }
    }

    // Properties

    private LoggingAdapter log = Logging.getLogger(getContext().getSystem(), getSelf());

    // Constructors

    private B() { }

    public static Props createActor() {
        return Props.create(B.class, B::new);
    }

    // Methods

    @Override
    public void onReceive(Object message) throws Throwable {
        if (message instanceof RequestRefMessage) {
            receiveRequestRef((RequestRefMessage) message);
        } else {
            log.warning("Received unknown message from {}: {}", getSender(), message);
        }
    }

    private void receiveRequestRef(RequestRefMessage message) {
        int id = message.getId();
        ActorRef reference = message.getReference();

        log.info("Received message from {}: id={}, reference={}", getSender(), id, reference);
        CompletableFuture<C.Response> future = new CompletableFuture<C.Response>();
        Patterns.pipe(future, getContext().getSystem().getDispatcher()).to(reference);
        future.complete(new C.Response(id));
    }
}