package demo;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedAbstractActor;

import akka.event.Logging;
import akka.event.LoggingAdapter;

import demo.B;

public final class A extends UntypedAbstractActor {

    // Messages

    public static final class TwoRefMessage {

        private final ActorRef ref1;
        private final ActorRef ref2;

        public TwoRefMessage(ActorRef ref1, ActorRef ref2) {
            this.ref1 = ref1;
            this.ref2 = ref2;
        }

        public ActorRef getFirstReference() {
            return ref1;
        }

        public ActorRef getSecondReference() {
            return ref2;
        }

    }

    // Properties

    private ActorRef actorB = null;
    private ActorRef actorC = null;

    private LoggingAdapter log = Logging.getLogger(getContext().getSystem(), getSelf());

    // Contructors

    private A() { }

    public static Props createActor() {
        return Props.create(A.class, A::new);
    }

    // Methods

    @Override
    public void onReceive(Object message) throws Throwable {
        if (message instanceof TwoRefMessage) {
            receiveTwoRef((TwoRefMessage) message);
        } else {
            log.warning("Received unknown message from {}: {}", getSender(), message);
        }
    }

    private void receiveTwoRef(TwoRefMessage message) {
        ActorRef ref1 = message.getFirstReference();
        ActorRef ref2 = message.getSecondReference();
        
        log.info("Received message from {}: ref1={}, ref2={}", getSender(), ref1, ref2);
        this.actorB = ref1;
        this.actorC = ref2;

        sendRequest();
    }

    private void sendRequest() {
        actorB.tell(new B.RequestRefMessage(10, actorC), getSelf());
    }

}