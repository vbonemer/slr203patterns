package demo;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedAbstractActor;

import akka.event.Logging;
import akka.event.LoggingAdapter;

public final class C extends UntypedAbstractActor {

    // Messages

    public static final class Response {

        private final int id;

        public Response(int id) {
            this.id = id;
        }
        
        public int getId() { return id; }

    }

    // Properties

    private final LoggingAdapter log = Logging.getLogger(getContext().getSystem(), getSelf());

    // Constructors

    private C() { }

    public static Props createActor() {
        return Props.create(C.class, C::new);
    }

    // Methods

    @Override
    public void onReceive(Object message) throws Throwable {
        if (message instanceof Response) {
            receiveResponse((Response) message);
        } else {
            log.warning("Received unknown message from {}: {}", getSender(), message);
        }
    }

    private void receiveResponse(Response message) {
        log.info("Received message from {}: id={}", getSender(), message.getId());
    }
}