package demo;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;

public final class RespondTo {

    public static void main(String[] args) {
        // Create System
        ActorSystem system = ActorSystem.create("system");

        // Create A
        ActorRef actorA = system.actorOf(A.createActor(), "a");
        // Create B
        ActorRef actorB = system.actorOf(B.createActor(), "b");
        // Create C
        ActorRef actorC = system.actorOf(C.createActor(), "c");

        // Send B and C refs to A
        actorA.tell(new A.TwoRefMessage(actorB, actorC), ActorRef.noSender());


        // We wait 5 seconds before ending system (by default)
        // But this is not the best solution.
        try {
            waitBeforeTerminate();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            system.terminate();
        }
    }

    public static void waitBeforeTerminate() throws InterruptedException {
        Thread.sleep(5000);
    }
}