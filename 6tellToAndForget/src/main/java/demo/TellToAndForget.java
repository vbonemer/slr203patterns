package demo;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;

import demo.Messages;

public class TellToAndForget {

    public static void main(String[] args) {

        final ActorSystem system = ActorSystem.create("system");

        // Create A
        final ActorRef a = system.actorOf(A.createActor(), "a");

        // Create Transmitter
        final ActorRef transmitter = system.actorOf(Transmitter.createActor(), "transmitter");

        // Create B
        final ActorRef b = system.actorOf(B.createActor(), "b");

        // Send Transmitter and B references to A
        a.tell(new Messages.TwoRefMessage(transmitter, b), ActorRef.noSender());

        // Send "start" to A
        a.tell(new Messages.StringMessage("start"), ActorRef.noSender());


        // We wait 5 seconds before ending system (by default)
		// But this is not the best solution.
		try {
			waitBeforeTerminate();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			system.terminate();
		}
	}

	public static void waitBeforeTerminate() throws InterruptedException {
		Thread.sleep(5000);
	}

	public static void sleepFor(int sec) {
		try {
			Thread.sleep(sec * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}

