package demo;

import akka.actor.ActorRef;

public class Messages {

    static public class StringMessage {

        private String content;

        public StringMessage(String content) {
            this.content = content;
        }

        public String getContent() { return content; }

    }

    static public class TwoRefMessage {

        private ActorRef ref1;
        private ActorRef ref2;

        public TwoRefMessage(ActorRef ref1, ActorRef ref2) {
            this.ref1 = ref1;
            this.ref2 = ref2;
        }

        public ActorRef getFirstReference() {
            return ref1;
        }

        public ActorRef getSecondReference() {
            return ref2;
        }

    }

    static public class StringRefMessage {

        private String str;
        private ActorRef reference;

        public StringRefMessage(String str, ActorRef ref) {
            this.str = str;
            this.reference = ref;
        }

        public String getString() {
            return str;
        }

        public ActorRef getSelf() {
            return reference;
        }

    }

}