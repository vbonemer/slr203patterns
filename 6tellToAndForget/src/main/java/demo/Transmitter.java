package demo;

import akka.actor.Props;
import akka.actor.UntypedAbstractActor;
import akka.actor.ActorRef;
import akka.event.Logging;
import akka.event.LoggingAdapter;

import demo.Messages;

public class Transmitter extends UntypedAbstractActor {

    private final LoggingAdapter log = Logging.getLogger(getContext().getSystem(), this);

    public Transmitter() { }

    // Static function creating actor
    public static Props createActor() {
        return Props.create(Transmitter.class, () -> {
            return new Transmitter();
        });
    }

    @Override
	public void onReceive(Object message) throws Throwable {
        if (message instanceof Messages.StringRefMessage) {
            Messages.StringRefMessage myMessage = (Messages.StringRefMessage) message;
            receivedStringRef(myMessage);
        }
    }

    private void receivedStringRef(Messages.StringRefMessage message) {
        ActorRef dest = message.getSelf();
        String content = message.getString();

        log.info("[{}] Received message from {}: ref={}, str={}", getSelf(), getSender(), dest, content);        
        dest.tell(new Messages.StringMessage(content), getSender());
    }

}