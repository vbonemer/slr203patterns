package demo;

import akka.actor.Props;
import akka.actor.UntypedAbstractActor;
import akka.actor.ActorRef;
import akka.event.Logging;
import akka.event.LoggingAdapter;

import demo.Messages;

public class A extends UntypedAbstractActor {

    private final LoggingAdapter log = Logging.getLogger(getContext().getSystem(), this);

    private ActorRef transmitter = null;
    private ActorRef actorB = null;

    public A() {}

    // Static function creating actor
    public static Props createActor() {
        return Props.create(A.class, () -> {
            return new A();
        });
    }

    @Override
	public void onReceive(Object message) throws Throwable {
		if (message instanceof Messages.TwoRefMessage) {
            Messages.TwoRefMessage myMessage = (Messages.TwoRefMessage) message;
            receivedTwoRef(myMessage);
            return;
        }
        if (message instanceof Messages.StringMessage) {
            Messages.StringMessage myMessage = (Messages.StringMessage) message;
            receivedString(myMessage);
            return;
        }
    }
    
    private void receivedTwoRef(Messages.TwoRefMessage message) {
        transmitter = message.getFirstReference();
        actorB = message.getSecondReference();
        log.info("[{}] Received message from {}: ref1={}, ref2={}", getSelf(), getSender(), transmitter, actorB);
    }

    private void receivedString(Messages.StringMessage message) {
        String content = message.getContent();
        log.info("[{}] Received message from {}: content={}", getSelf(), getSender(), content);

        if (!content.equals("start")) { return; }
        
        // Send message to Transmitter
        Messages.StringRefMessage newMessage = new Messages.StringRefMessage("hello", actorB);
        transmitter.tell(newMessage, getSelf());
    }

}