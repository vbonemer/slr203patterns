package demo;

import akka.actor.Props;
import akka.actor.UntypedAbstractActor;
import akka.actor.ActorRef;
import akka.event.Logging;
import akka.event.LoggingAdapter;

import demo.Messages;

public class B extends UntypedAbstractActor {

    private final LoggingAdapter log = Logging.getLogger(getContext().getSystem(), this);

    public B() { }

    // Static function creating actor
    public static Props createActor() {
        return Props.create(B.class, () -> {
            return new B();
        });
    }

    @Override
	public void onReceive(Object message) throws Throwable {
        if (message instanceof Messages.StringMessage) {
            Messages.StringMessage myMessage = (Messages.StringMessage) message;
            receivedString(myMessage);
        }
    }

    private void receivedString(Messages.StringMessage message) {
        ActorRef sender = getSender();

        log.info("[{}] Received message from {}: {}", getSelf(), getSender(), message.getContent());
        sender.tell(new Messages.StringMessage("hi!"), getSelf());
    }

}