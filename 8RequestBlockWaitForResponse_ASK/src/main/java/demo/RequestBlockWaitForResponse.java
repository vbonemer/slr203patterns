package demo;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;

public final class RequestBlockWaitForResponse {

    private static ActorSystem system = ActorSystem.create("system");

    public static void main(String[] args) {
        // Create a
        ActorRef a = system.actorOf(A.createActor(), "a");
        // Create b
        ActorRef b = system.actorOf(B.createActor(), "b");
        // Send b ref to a        
        a.tell(new A.RefMessage(b), ActorRef.noSender());


        // We wait 5 seconds before ending system (by default)
        // But this is not the best solution.
        try {
            waitBeforeTerminate();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            system.terminate();
        }
    }

    public static void waitBeforeTerminate() throws InterruptedException {
        Thread.sleep(5000);
    }
}
