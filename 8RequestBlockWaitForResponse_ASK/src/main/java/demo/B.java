package demo;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedAbstractActor;

import akka.event.Logging;
import akka.event.LoggingAdapter;

import demo.Messages.Request;
import demo.Messages.Response;

public final class B extends UntypedAbstractActor {

    // Properties

    private LoggingAdapter log = Logging.getLogger(getContext().getSystem(), getSelf());

    // Constructors

    private B() { }

    public static Props createActor() {
        return Props.create(B.class, B::new);
    }

    // Methods

    @Override
    public void onReceive(Object message) throws Throwable {
        if (message instanceof Request) {
            receiveRequest((Request) message);
        } else {
            log.warning("Received unknown message from {}: {}", getSender(), message);
        }
    }

    private void receiveRequest(Request message) {
        int id = message.getId();
        log.info("Received message from {}: id={}", getSender(), id);
        getSender().tell(new Response(id), getSelf());
    }

}