package demo;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedAbstractActor;

import akka.event.Logging;
import akka.event.LoggingAdapter;

import akka.pattern.Patterns;
import scala.concurrent.Future;
import scala.concurrent.Await;
import akka.util.Timeout;
import java.time.Duration;

import demo.Messages.Request;
import demo.Messages.Response;

public final class A extends UntypedAbstractActor {

    // Messages

    static public final class RefMessage {

        private final ActorRef reference;

        public RefMessage(ActorRef ref) {
            this.reference = ref;
        }

        public ActorRef getReference() { return this.reference; }

    }

    // Properties

    private ActorRef actorB = null;

    private LoggingAdapter log = Logging.getLogger(getContext().getSystem(), getSelf());

    // Constructors

    private A() { }

    public static Props createActor() {
        return Props.create(A.class, A::new);
    }

    // Methods

    @Override
    public void onReceive(Object message) throws Throwable {
        if (message instanceof RefMessage) {
            receiveReference((RefMessage) message);
        } else {
            log.warning("Received unknown message from {}: {}", getSender(), message);
        }
    }

    private void receiveReference(RefMessage message) {
        log.info("Received message from {}: ref={}", getSender(), message.getReference());
        this.actorB = message.getReference();
        sendRequests();
    }

    private void sendRequests() {
        Timeout timeout = Timeout.create(Duration.ofSeconds(5));
        Future<Object> future = Patterns.ask(actorB, new Request(10), timeout);
        try {
            Response response = (Response) Await.result(future, timeout.duration());
            log.info("Received response from {}: id={}", actorB, response.getId());
        } catch (Exception e) {
            log.error("Received Exception: {}", e);
            e.printStackTrace();
        }

        Future<Object> future2 = Patterns.ask(actorB, new Request(20), timeout);
        try {
            Response response2 = (Response) Await.result(future2, timeout.duration());
            log.info("Received response from {}: id={}", actorB, response2.getId());
        } catch (Exception e) {
            log.error("Received Exception: {}", e);
            e.printStackTrace();
        }
    }

}