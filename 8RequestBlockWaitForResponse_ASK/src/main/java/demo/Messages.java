package demo;

import akka.actor.ActorRef;

public final class Messages {

    public static final class Request {

        private final int id;

        public Request(int id) {
            this.id = id;
        }

        public int getId() { return this.id; }

    }

    public static final class Response {

        private final int id;

        public Response(int id) {
            this.id = id;
        }

        public int getId() { return this.id; }

    }

}