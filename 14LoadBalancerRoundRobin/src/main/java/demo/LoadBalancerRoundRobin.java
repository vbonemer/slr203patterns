package demo;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;

import java.time.Duration;

public final class LoadBalancerRoundRobin {
    
    public static void main(String[] args) {
        // Create the system
        ActorSystem system = ActorSystem.create("system");
        
        // Create LoadBalancer
        ActorRef loadBalancer = system.actorOf(LoadBalancer.createActor(), "LoadBalancer");
        // Create A
        ActorRef actorA = system.actorOf(Sender.createActor(4), "A");
        // Create B
        ActorRef actorB = system.actorOf(Receiver.createActor(false), "B");
        // Create C
        ActorRef actorC = system.actorOf(Receiver.createActor(true), "C");

        // Send LoadBalancer reference to A
        actorA.tell(loadBalancer, ActorRef.noSender());

        // Send LoadBalancer reference to B
        system.getScheduler().scheduleOnce(
            Duration.ofMillis(100), actorB, loadBalancer, 
            system.getDispatcher(), ActorRef.noSender()
        );

        // Send LoadBalancer reference to C
        system.getScheduler().scheduleOnce(
            Duration.ofMillis(200), actorC, loadBalancer, 
            system.getDispatcher(), ActorRef.noSender()
        );


        // We wait 5 seconds before ending system (by default)
        // But this is not the best solution.
        try {
            waitBeforeTerminate();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            system.terminate();
        }
    }

    private static void waitBeforeTerminate() throws InterruptedException {
        Thread.sleep(5000);
    }

}