package demo;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.AbstractActor;

import akka.event.Logging;
import akka.event.LoggingAdapter;

import java.util.LinkedList;

public final class LoadBalancer extends AbstractActor {

    // Messages

    public static final class Join { }

    public static final class Unjoin { }

    // Properties

    private LinkedList<ActorRef> receivers = new LinkedList<ActorRef>();

    private LoggingAdapter log = Logging.getLogger(getContext().getSystem(), getSelf());

    // Constructors

    private LoadBalancer() { }

    public static Props createActor() {
        return Props.create(LoadBalancer.class, LoadBalancer::new);
    }

    // Methods

    @Override
    public Receive createReceive() {
        return receiveBuilder()
            .match(Join.class, this::receiveJoin)
            .match(Unjoin.class, this::receiveUnjoin)
            .match(String.class, this::receiveString)
            .build();
    }
    
    private void receiveJoin(Join message) {
        ActorRef sender = getSender();
        log.info("Received message from {}: join", sender);
        receivers.add(sender);
    }

    private void receiveUnjoin(Unjoin message) {
        ActorRef sender = getSender();
        log.info("Received message from {}: unjoin", sender);
        receivers.remove(sender);
    }

    private void receiveString(String message) {
        ActorRef sender = getSender();
        log.info("Received message from {}: content={}", sender, message);
        ActorRef next = receivers.pollFirst();
        try {
            next.tell(message, sender);
            receivers.add(next);
        } catch (NullPointerException e) {
            log.error("Received message {} but no receivers are registered.", message);
        }
    }

}