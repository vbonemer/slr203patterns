package demo;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.AbstractActor;

import akka.event.Logging;
import akka.event.LoggingAdapter;

public final class Receiver extends AbstractActor {

    // Properties

    private ActorRef balancer = null;

    private boolean unsubscribeAfterReceive;

    private LoggingAdapter log = Logging.getLogger(getContext().getSystem(), getSelf());

    // Constructors

    private Receiver(boolean unsubscribeAfterReceive) {
        this.unsubscribeAfterReceive = unsubscribeAfterReceive;
    }

    public static Props createActor(boolean unsubscribeAfterReceive) {
        return Props.create(Receiver.class, () -> {
            return new Receiver(unsubscribeAfterReceive);
        });
    }

    // Methods

    @Override
    public Receive createReceive() {
        return receiveBuilder()
            .match(ActorRef.class, this::receiveRef)
            .match(String.class, this::receiveString)
            .build();
    }

    private void receiveRef(ActorRef message) {
        log.info("Received message from {}: ref={}", getSender(), message);
        this.balancer = message;
        subscribe();
    }

    private void receiveString(String message) {
        log.info("Received message from {}: content={}", getSender(), message);
        
        if (!unsubscribeAfterReceive) { return; }
        
        unsubscribe();
    }

    private void subscribe() {
        balancer.tell(new LoadBalancer.Join(), getSelf());
    }

    private void unsubscribe() {
        balancer.tell(new LoadBalancer.Unjoin(), getSelf());
    }
}