package demo;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.AbstractActor;

import akka.actor.Scheduler;
import java.time.Duration;

import akka.event.Logging;
import akka.event.LoggingAdapter;

import akka.actor.Cancellable;

public final class Sender extends AbstractActor {

    // Messages

    public static final class SendMessage { }

    // Properties

    private int messageCount;

    private int sentMessages = 0;

    private ActorRef destination = null;

    private Cancellable schedule = null;

    private LoggingAdapter log = Logging.getLogger(getContext().getSystem(), getSelf());

    // Constructors

    private Sender(int messageCount) {
        this.messageCount = messageCount;
    }

    public static Props createActor(int messageCount) {
        return Props.create(Sender.class, () -> {
            return new Sender(messageCount);
        });
    }

    // Methods

    @Override
    public Receive createReceive() {
        return receiveBuilder()
            .match(SendMessage.class, this::receiveSendMessage)
            .match(ActorRef.class, this::receiveRef)
            .build();
    }

    private void receiveSendMessage(SendMessage message) {
        log.info("Received message from {}: send", getSender());
        sendMessage();
    }

    private void receiveRef(ActorRef message) {
        log.info("Received message from {}: ref={}", getSender(), message);
        this.destination = message;

        scheduleMessages();
    }

    private void scheduleMessages() {
        schedule = getContext().getSystem().getScheduler().schedule(
            Duration.ofSeconds(1), Duration.ofMillis(500), getSelf(), new SendMessage(), 
            getContext().getSystem().getDispatcher(), ActorRef.noSender()
        );
    }

    private void sendMessage() {
        destination.tell("m" + (++sentMessages), getSelf());

        if (sentMessages == messageCount) {
            schedule.cancel();
        }
    }

}