package demo;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.AbstractActor;

import akka.event.Logging;
import akka.event.LoggingAdapter;

import java.time.Duration;
import akka.actor.Cancellable;

public final class Publisher extends AbstractActor {

    // Messages

    public static final class SaveReference {

        private final ActorRef reference;

        public SaveReference(ActorRef ref) {
            this.reference = ref;
        }

        public ActorRef getReference() {
            return reference;
        }

    }

    public static final class SendMessage { }

    // Properties

    private String messageContent;
    private Duration startDelay;
    private int repeatCount;
    private Duration frequency;

    private int sentMessages = 0;

    private Cancellable schedule = null;

    private ActorRef topic = null;

    private LoggingAdapter log = Logging.getLogger(getContext().getSystem(), getSelf());

    // Constructors

    private Publisher(String message, Duration startDelay, int repeatCount, Duration frequency) {
        this.messageContent = message;
        this.startDelay = startDelay;
        this.repeatCount = repeatCount;

        if (frequency == null) {
            this.frequency = Duration.ZERO;
        } else {
            this.frequency = frequency;
        }
    }

    public static Props createActor(String message, Duration startDelay, int repeatCount, Duration frequency) {
        return Props.create(Publisher.class, () -> {
            return new Publisher(message, startDelay, repeatCount, frequency);
        });
    }

    // Methods

    @Override
    public Receive createReceive() {
        return receiveBuilder()
            .match(SaveReference.class, this::receiveSaveReference)
            .match(SendMessage.class, this::receiveSendMessage)
            .build();
    }

    private void receiveSaveReference(SaveReference message) {
        ActorRef reference = message.getReference();
        log.info("Received message from {}: ref={}", getSender(), reference);

        this.topic = reference;

        scheduleMessage();
    }

    private void receiveSendMessage(SendMessage message) {
        log.info("Received send message");
        String content = messageContent;
        if (sentMessages++ != 0) { content += sentMessages; }
        
        topic.tell(new StringMessage(content), getSelf());
        
        if (sentMessages > repeatCount) {
            schedule.cancel();
        }
    }

    private void scheduleMessage() {
        schedule = getContext().getSystem().getScheduler().schedule(
            startDelay, frequency, getSelf(), new SendMessage(), 
            getContext().getSystem().getDispatcher(), ActorRef.noSender()
        );
    }
}