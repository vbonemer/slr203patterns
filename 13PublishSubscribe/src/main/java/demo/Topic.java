package demo;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.AbstractActor;

import akka.event.Logging;
import akka.event.LoggingAdapter;

import java.util.HashSet;

public final class Topic extends AbstractActor {

    // Messages

    public static final class Subscribe { }

    public static final class Unsubscribe { }

    // Properties

    private HashSet<ActorRef> subscribers = new HashSet<ActorRef>();

    private LoggingAdapter log = Logging.getLogger(getContext().getSystem(), getSelf());

    // Constructors

    private Topic() { }

    public static Props createActor() {
        return Props.create(Topic.class, Topic::new);
    }

    // Methods

    @Override
    public Receive createReceive() {
        return receiveBuilder()
            .match(Subscribe.class, this::receiveSubscribe)
            .match(Unsubscribe.class, this::receiveUnsubscribe)
            .match(StringMessage.class, this::receiveString)
            .build();
    }

    private void receiveSubscribe(Subscribe message) {
        ActorRef subscriber = getSender();

        log.info("Received message from {}: subscribe", subscriber);
        subscribers.add(subscriber);
    }

    private void receiveUnsubscribe(Unsubscribe message) {
        ActorRef subscriber = getSender();

        log.info("Received message from {}: unsubscribe", subscriber);
        subscribers.remove(subscriber);
    }

    private void receiveString(StringMessage message) {
        log.info("Received message from {}: content={}", getSender(), message.getContent());

        for (ActorRef subscriber : subscribers) {
            subscriber.tell(message, getSelf());
        }
    }

}