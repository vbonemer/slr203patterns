package demo;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;

import java.time.Duration;

public final class PublishSubscribe {
    
    public static void main(String[] args) {
        // Create the system
        ActorSystem system = ActorSystem.create("system");
        
        // Create A
        ActorRef actorA = system.actorOf(Subscriber.createActor(), "A");
        // Create B
        ActorRef actorB = system.actorOf(Subscriber.createActor(), "B");
        // Create C
        ActorRef actorC = system.actorOf(Subscriber.createActor(), "C");

        // Create Publisher1
        ActorRef publisher1 = system.actorOf(Publisher.createActor(
            "hello", Duration.ofSeconds(1), 1, Duration.ofSeconds(2)
        ), "Publisher1");
        // Create Publisher2
        ActorRef publisher2 = system.actorOf(Publisher.createActor(
            "world", Duration.ofSeconds(2), 0, null
        ), "Publisher2");

        // Create Topic1
        ActorRef topic1 = system.actorOf(Topic.createActor(), "Topic1");
        // Create Topic2
        ActorRef topic2 = system.actorOf(Topic.createActor(), "Topic2");

        // Send Topic1 Reference to Publisher1
        publisher1.tell(new Publisher.SaveReference(topic1), ActorRef.noSender());
        // Send Topic2 Reference to Publisher2
        publisher2.tell(new Publisher.SaveReference(topic2), ActorRef.noSender());

        // Send Topic1 reference to A
        actorA.tell(new Subscriber.SaveTopic(topic1, true), ActorRef.noSender());
        // Send Topic1 reference to B
        actorB.tell(new Subscriber.SaveTopic(topic1, false), ActorRef.noSender());
        // Send Topic2 reference to B
        actorB.tell(new Subscriber.SaveTopic(topic2, false), ActorRef.noSender());
        // Send Topic2 reference to C
        actorC.tell(new Subscriber.SaveTopic(topic2, false), ActorRef.noSender());


        // We wait 5 seconds before ending system (by default)
        // But this is not the best solution.
        try {
            waitBeforeTerminate();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            system.terminate();
        }
    }

    private static void waitBeforeTerminate() throws InterruptedException {
        Thread.sleep(5000);
    }

}