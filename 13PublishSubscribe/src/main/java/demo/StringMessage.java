package demo;

import akka.actor.ActorRef;

public final class StringMessage {

    private final String content;

    public StringMessage(String content) {
        this.content = content;
    }

    public String getContent() { return content; }

}

