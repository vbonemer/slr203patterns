package demo;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.AbstractActor;

import akka.event.Logging;
import akka.event.LoggingAdapter;

import java.io.Serializable;
import java.util.HashMap;

public final class Subscriber extends AbstractActor {

    // Messages

    public static final class SaveTopic implements Serializable {

        static final long serialVersionUID = 202101161756L;

        private final ActorRef reference;
        private final boolean shouldUnsubscribeAfterReceive;

        public SaveTopic(ActorRef reference, boolean unsubscribeAfterReceive) {
            this.reference = reference;
            this.shouldUnsubscribeAfterReceive = unsubscribeAfterReceive;
        }

        public ActorRef getReference() { return reference; }

        public boolean getShouldUnsubscribe() { return shouldUnsubscribeAfterReceive; }

    }

    // Properties

    private HashMap<ActorRef, Boolean> shouldUnsubscribe = new HashMap<ActorRef, Boolean>();

    private LoggingAdapter log = Logging.getLogger(getContext().getSystem(), getSelf());

    // Constructors

    private Subscriber() { }

    public static Props createActor() {
        return Props.create(Subscriber.class, Subscriber::new);
    }

    // Methods

    @Override
    public Receive createReceive() {
        return receiveBuilder()
            .match(SaveTopic.class, this::receiveSaveTopic)
            .match(StringMessage.class, this::receiveString)
            .build();
    }

    private void receiveSaveTopic(SaveTopic message) {
        ActorRef reference = message.getReference();
        boolean unsubscribe = message.getShouldUnsubscribe();
        log.info("Received message from {}: topic={}, unsub={}", getSender(), reference, unsubscribe);

        shouldUnsubscribe.put(reference, unsubscribe);

        subscribeToTopic(reference);
    }

    private void receiveString(StringMessage message) {
        ActorRef topic = getSender();
        log.info("Received message from {}: content={}", topic, message.getContent());

        if (shouldUnsubscribe.get(topic)) {
            unsubscribeToTopic(topic);
        }
    }

    private void subscribeToTopic(ActorRef topic) {
        topic.tell(new Topic.Subscribe(), getSelf());
    }

    private void unsubscribeToTopic(ActorRef topic) {
        topic.tell(new Topic.Unsubscribe(), getSelf());
        shouldUnsubscribe.remove(topic);
    }

}