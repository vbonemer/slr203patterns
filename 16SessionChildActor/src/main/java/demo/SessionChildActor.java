package demo;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;

public final class SessionChildActor {
    
    public static void main(String[] args) {
        // Create the system
        ActorSystem system = ActorSystem.create("system");
        
        // Create Client1
        ActorRef client1 = system.actorOf(Client.createActor(), "Client1");

        // Create SessionManager
        ActorRef sessionManager = system.actorOf(SessionManager.createActor(), "SessionManager");

        // Send SessionManager reference to Client1
        client1.tell(sessionManager, ActorRef.noSender());

        // We wait 5 seconds before ending system (by default)
        // But this is not the best solution.
        try {
            waitBeforeTerminate();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            system.terminate();
        }
    }

    private static void waitBeforeTerminate() throws InterruptedException {
        Thread.sleep(5000);
    }

}