package demo;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.AbstractActor;

import akka.event.Logging;
import akka.event.LoggingAdapter;

public final class Session extends AbstractActor {

    // Properties

    private int responseLimit;

    private int responseCount = 0;

    private final LoggingAdapter log = Logging.getLogger(getContext().getSystem(), getSelf());

    // Constructors

    private Session(int responseLimit) {
        this.responseLimit = responseLimit;
    }

    public static Props createActor(int responseLimit) {
        return Props.create(Session.class, () -> {
            return new Session(responseLimit);
        });
    }

    // Methods

    @Override
    public Receive createReceive() {
        return receiveBuilder()
            .match(String.class, this::receiveString)
            .build();
    }

    @Override
    public void postStop() {
        log.info("Stopped");
    }

    private void receiveString(String message) {
        ActorRef sender = getSender();
        log.info("Received message from {}: content={}", sender.path().name(), message);

        int number;
        try {
            number = Integer.parseInt(message.substring(1, 2)) + 1;
        } catch (Exception e) {
            log.error("\tInvalid message format. Expected 'm<int>'. Got {}", message);
            return;
        }

        if (responseCount >= responseLimit) {
            return;
        }
        responseCount++;
        sender.tell("m" + number, getSelf());
    }

}
