package demo;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.AbstractActor;

import akka.event.Logging;
import akka.event.LoggingAdapter;

import java.time.Duration;

public final class Client extends AbstractActor {

    // Properties

    private ActorRef manager = null;

    private ActorRef session = null;

    private AbstractActor.Receive defaultRefHandler = receiveBuilder()
        .match(ActorRef.class, this::receiveReference)
        .match(String.class, this::receiveString)
        .build();

    private LoggingAdapter log = Logging.getLogger(getContext().getSystem(), getSelf());

    // Constructors

    private Client() { }

    public static Props createActor() {
        return Props.create(Client.class, Client::new);
    }

    // Methods

    @Override
    public Receive createReceive() {
        return receiveBuilder()
            .match(ActorRef.class, message -> {
                log.info("Received message from {}: ref={}", getSender().path().name(), message.path().name());
                this.manager = message;
                getContext().become(defaultRefHandler);
                manager.tell(new SessionManager.CreateSession(), getSelf());
            })
            .build();
    }
    
    private void receiveReference(ActorRef message) {
        log.info("Received message from {}: ref={}", getSender().path().name(), message.path().name());
        this.session = message;
        sendMessages();
    }

    private void sendMessages() {
        getContext().getSystem().getScheduler().scheduleOnce(
            Duration.ofMillis(100), session, "m1", 
            getContext().getSystem().getDispatcher(), getSelf()
        );

        getContext().getSystem().getScheduler().scheduleOnce(
            Duration.ofMillis(300), session, "m3", 
            getContext().getSystem().getDispatcher(), getSelf()
        );

        getContext().getSystem().getScheduler().scheduleOnce(
            Duration.ofMillis(1000), manager, new SessionManager.EndSession(session), 
            getContext().getSystem().getDispatcher(), getSelf()
        );
    }

    private void receiveString(String message) {
        log.info("Received message from {}: content={}", getSender().path().name(), message);
    }

}