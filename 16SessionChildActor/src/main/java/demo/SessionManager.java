package demo;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.AbstractActor;

import akka.event.Logging;
import akka.event.LoggingAdapter;

import java.util.HashSet;

public final class SessionManager extends AbstractActor {

    // Messages

    public static final class CreateSession { }

    public static final class EndSession {
        public ActorRef session;

        public EndSession(ActorRef session) {
            this.session = session;
        }
    }

    // Other

    private static final class SessionIdProvider {
        private int nextValidId = 0;

        private int getValidId() {
            return this.nextValidId++;
        }
    }

    // Properties

    private HashSet<ActorRef> sessions = new HashSet<ActorRef>();

    private SessionIdProvider sessionIdProvider = new SessionIdProvider();

    private LoggingAdapter log = Logging.getLogger(getContext().getSystem(), getSelf());

    // Constructors

    private SessionManager() { }

    public static Props createActor() {
        return Props.create(SessionManager.class, SessionManager::new);
    }

    // Methods

    @Override
    public Receive createReceive() {
        return receiveBuilder()
            .match(CreateSession.class, this::receiveCreateSession)
            .match(EndSession.class, this::receiveEndSession)
            .build();
    }
    
    private void receiveCreateSession(CreateSession message) {
        ActorRef sender = getSender();
        log.info("Received message from {}: createSession", sender.path().name());
        
        // Create session
        ActorRef session = getContext().actorOf(
            Session.createActor(1), "Session" + sessionIdProvider.getValidId()
        );
        // Add session to hashset
        sessions.add(session);
        // Send back session reference
        sendBackReference(session);
    }

    private void receiveEndSession(EndSession message) {
        ActorRef sender = getSender();
        log.info("Received message from {}: endSession", sender.path().name());
        
        endSession(message.session);
    }

    private void sendBackReference(ActorRef reference) {
        getSender().tell(reference, getSelf());
    }

    private void endSession(ActorRef session) {
        if (!sessions.contains(session)) {
            log.error(
                "Received EndSession from {} but the reference {} is not a valid session",
                getSender().path().name(), session
            );
            return;
        }

        sessions.remove(session);
        getContext().stop(session);
    }

}